'use strict';

import React, { Component } from 'react'
import {
	ScrollView,
	StyleSheet,
	Text,
	TextInput,
	View,
	TouchableHighlight,
	TouchableOpacity,
	ActivityIndicator,
	Image,
	Button
} from 'react-native';
import { StackNavigator } from 'react-navigation';

var styles = StyleSheet.create({
	fullContainer: {
		backgroundColor: 'white'
	},
	description: {
		marginBottom: 20,
		fontSize: 18,
		textAlign: 'center',
		color: '#656565'
	},
	container: {
		flex: 1,
		flexDirection: 'column',
		height: 'auto',
		margin: 20,
		alignItems: 'center',
		backgroundColor: 'white',
	},
	containerHeader: {
		flexDirection: 'row',
	},
	containerHeaderLeftPlaceHolder: {
		flex: 1,
		paddingLeft: 0
	},
	separator: {
		height: 1,
		marginRight: 20,
		marginLeft: 20,
		backgroundColor: 'lightgray'
	},
	alignedFont: {
		fontFamily: 'System',
		textAlign: 'center',
		color: 'black'
	},
	blockTitle: {
		flex: 4,
		fontSize: 12,
		height: 20
	},
	blockHyperLink: {
		flex: 1,
		fontSize: 12,
		paddingRight: 0,
		textAlign: 'right',
		textDecorationLine: 'underline',
		color: 'rgb(23, 125, 251)',
	},
	currentSavingsValues: {
		fontSize: 16
	},
	nextPaymentsContainer: {
		flex: 1,
		flexDirection: 'row'
	}
});

class DashboardPage extends Component {
	static navigationOptions = {
		title: 'Dashboard',
	};

	render() {
		let i;
		i = 3;
		const { navigate } = this.props.navigation;
		return (
			<ScrollView style={styles.fullContainer}>
				<View style={styles.container}>
					<View style={styles.containerHeader}>
						<View style={styles.containerHeaderLeftPlaceHolder} />
						<Text style={[styles.alignedFont, styles.blockTitle]}>My current savings</Text>
						<TouchableOpacity onPress={() => navigate('MyCurrentSavingsDetail')}>
							<Text style={[styles.alignedFont, styles.blockHyperLink]}>History</Text>
						</TouchableOpacity>
					</View>
					<Text style={[styles.alignedFont, { fontSize: 24 }]}>122 458 EUR</Text>
					<View style={[styles.containerHeader, { paddingTop: 15 }]}>
						<View style={{ flex: 1 }}>
							<Text style={styles.alignedFont}>OFFTRACK</Text>
							<Text style={[styles.alignedFont, styles.currentSavingsValues]}>800 EUR</Text>
						</View>
						<View style={{ flex: 1 }}>
							<Text style={styles.alignedFont}>YIELD(Last 365 days)</Text>
							<Text style={[styles.alignedFont, styles.currentSavingsValues]}>+375 EUR</Text>
						</View>
					</View>
				</View>
				<View style={styles.separator} />
				<View style={styles.container}>
					<View style={styles.containerHeader}>
						<View style={styles.containerHeaderLeftPlaceHolder} />
						<Text style={[styles.alignedFont, styles.blockTitle]}>Next payments</Text>
						<Text style={[styles.alignedFont, styles.blockHyperLink]}>All</Text>
					</View>
					<View style={{ flex: 1, flexDirection: 'row', paddingTop: 15 }}>
						<View style={{ flex: 1, flexDirection: 'row', borderColor: 'lightgray', borderWidth: 1, marginRight: 5 }}>
							<View style={{ flex: 1, flexDirection: 'column' }}>
								<View style={{ flex: 1, flexDirection: 'row' }}>
									<View style={{ flex: 1, justifyContent: 'center', marginTop: 5 }}>
										<Text style={{ textAlign: 'center', fontSize: 26, color: 'black' }}>25</Text>
										<Text style={{ textAlign: 'center', fontSize: 12, color: 'black' }}>Nov 2017</Text>
									</View>
									<Image source={require('./img/icon.png')} style={{ marginTop: 10, flex: 1, resizeMode: 'contain' }} />
								</View>
								<View style={{ height: 1, width: 'auto', backgroundColor: 'lightgray', marginTop: 5, marginLeft: 10, marginRight: 10 }} />
								<Text style={{ textAlign: 'center', fontSize: 16, color: 'black', marginTop: 5, marginBottom: 5 }}>+371 EUR</Text>
							</View>
						</View>
						<View style={{ flex: 1, flexDirection: 'row', borderColor: 'lightgray', borderWidth: 1, marginRight: 5 }}>
							<View style={{ flex: 1, flexDirection: 'column' }}>
								<View style={{ flex: 1, flexDirection: 'row' }}>
									<View style={{ flex: 1, justifyContent: 'center', marginTop: 5 }}>
										<Text style={{ textAlign: 'center', fontSize: 26, color: 'black' }}>14</Text>
										<Text style={{ textAlign: 'center', fontSize: 12, color: 'black' }}>Dec 2017</Text>
									</View>
									<Image source={require('./img/icon.png')} style={{ marginTop: 10, flex: 1, resizeMode: 'contain' }} />
								</View>
								<View style={{ height: 1, width: 'auto', backgroundColor: 'lightgray', marginTop: 5, marginLeft: 10, marginRight: 10 }} />
								<Text style={{ textAlign: 'center', fontSize: 16, color: 'black', marginTop: 5, marginBottom: 5 }}>+371 EUR</Text>
							</View>
						</View>
					</View>
				</View>
				<View style={styles.separator} />
				<View style={styles.container}>
					<View style={styles.containerHeader}>
						<View style={styles.containerHeaderLeftPlaceHolder} />
						<Text style={[styles.alignedFont, styles.blockTitle]}>Ongoing transactions</Text>
						<Text style={[styles.alignedFont, styles.blockHyperLink]}>All</Text>
					</View>
					<View style={{ flexDirection: 'row', paddingTop: 15 }}>
						<View style={{ flex: 1, flexDirection: 'row' }}>
							<View style={{ flex: 1, flexDirection: 'column' }}>
								<Text style={{ fontSize: 12, fontWeight: 'bold', textAlign: 'left', color: 'black' }}>ALL-RCMEUEQ</Text>
								<Text style={{ fontSize: 12, fontWeight: 'bold', textAlign: 'left', color: 'black' }}>140.09 EUR * 246 PCS</Text>
								<Text style={{ fontSize: 12, textAlign: 'left', color: 'gray' }}>Buy</Text>
							</View>
							<Text style={{ fontSize: 12, textAlign: 'left', color: 'gray' }}>34 454.76 EUR</Text>
						</View>
					</View>
					<View style={{ flexDirection: 'row', paddingTop: 15 }}>
						<View style={{ flex: 1, flexDirection: 'row' }}>
							<View style={{ flex: 1, flexDirection: 'column' }}>
								<Text style={{ fontSize: 12, fontWeight: 'bold', textAlign: 'left', color: 'black' }}>ALL-RCMEUEQ</Text>
								<Text style={{ fontSize: 12, fontWeight: 'bold', textAlign: 'left', color: 'black' }}>140.09 EUR * 246 PCS</Text>
								<Text style={{ fontSize: 12, textAlign: 'left', color: 'gray' }}>Buy</Text>
							</View>
							<Text style={{ fontSize: 12, textAlign: 'left', color: 'gray' }}>34 454.76 EUR</Text>
						</View>
					</View>
				</View>
				<View style={styles.separator} />
				<View style={styles.container}>
					<View style={styles.containerHeader}>
						<View style={styles.containerHeaderLeftPlaceHolder} />
						<Text style={[styles.alignedFont, styles.blockTitle]}>My yearly saving plan</Text>
						<Text style={[styles.alignedFont, styles.blockHyperLink]}>Manage</Text>
					</View>
					<View style={{ flex: 1, flexDirection: 'row', paddingTop: 20 }}>
						<View style={{ flex: 1, flexDirection: 'column' }}>
							<View style={{ flexDirection: 'column', marginBottom: 10, alignItems: 'center' }}>
								<Text style={{ color: 'black', fontSize: 12 }}>PLANNED SAVING (1Y)</Text>
								<Text style={{ color: 'black', fontSize: 16 }}>24 000 EUR</Text>
							</View>
							<View style={{ flexDirection: 'column', marginBottom: 10, alignItems: 'center' }}>
								<Text style={{ color: 'black', fontSize: 12 }}>MONTHLY SAVING</Text>
								<Text style={{ color: 'black', fontSize: 16 }}>1 200 EUR</Text>
							</View>
							<View style={{ flexDirection: 'column', marginBottom: 10, alignItems: 'center' }}>
								<Text style={{ color: 'black', fontSize: 12 }}>OFFTRACK</Text>
								<Text style={{ color: 'black', fontSize: 16 }}>800 EUR</Text>
							</View>
						</View>
						<Image style={{ flex: 1, resizeMode: 'contain' }} source={require('./img/chart1.png')} />
					</View>
				</View>
				<View style={styles.separator} />
				<View style={styles.container}>
					<View style={styles.containerHeader}>
						<View style={styles.containerHeaderLeftPlaceHolder} />
						<Text style={[styles.alignedFont, styles.blockTitle]}>My portfolio</Text>
						<Text style={[styles.alignedFont, styles.blockHyperLink]}>Manage</Text>
					</View>
					<Text style={[styles.alignedFont, styles.blockTitle]}>OVERVIEW</Text>
					<View style={{ flex: 1, flexDirection: 'row', paddingTop: 10, alignItems: 'center' }}>
						<Image style={{ flex: 1, resizeMode: 'contain' }} source={require('./img/chart2.png')} />
						<View style={{ flex: 1, flexDirection: 'column' }}>
							<View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
								<Text style={{ color: 'black', fontSize: 12 }}>Investment</Text>
								<Text style={{ color: 'black', fontSize: 12 }}>15%</Text>
							</View>
							<View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
								<Text style={{ color: 'black', fontSize: 12 }}>Real estate</Text>
								<Text style={{ color: 'black', fontSize: 12 }}>21%</Text>
							</View>
							<View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
								<Text style={{ color: 'black', fontSize: 12 }}>Deposit</Text>
								<Text style={{ color: 'black', fontSize: 12 }}>50%</Text>
							</View>
							<View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
								<Text style={{ color: 'black', fontSize: 12 }}>Cash</Text>
								<Text style={{ color: 'black', fontSize: 12 }}>14%</Text>
							</View>
						</View>
					</View>
				</View>
				<View style={styles.separator} />
				<View style={styles.container}>
					<View style={styles.containerHeader}>
						<View style={styles.containerHeaderLeftPlaceHolder} />
						<Text style={[styles.alignedFont, styles.blockTitle]}>Improve your investments</Text>
						<Text style={[styles.alignedFont, styles.blockHyperLink]}>All</Text>
					</View>
					<Image source={require('./img/chat1.png')} style={{ marginTop: 10 }}>
						<Text style={{ color: 'rgb(23, 125, 251)', backgroundColor: 'transparent', paddingTop: 32, paddingLeft: 20, paddingRight: 30, lineHeight: 20 }}>Identifying your financial advice needs
						<Text style={{ color: 'black' }}> - Work out whether you need financial advice.</Text></Text>
					</Image>

				</View>
				<View style={styles.separator} />
				<View style={styles.container}>
					<View style={styles.containerHeader}>
						<View style={styles.containerHeaderLeftPlaceHolder} />
						<Text style={[styles.alignedFont, styles.blockTitle]}>My upcoming goals</Text>
						<Text style={[styles.alignedFont, styles.blockHyperLink]}>Manage</Text>
					</View>
					<Image source={require('./img/goals.png')} style={{ marginTop: 10 }} />
					<Text style={{ color: 'black' }}>Travel to Everest - 25%</Text>
					<Text style={{ fontSize: 10, color: 'black' }}>In 4 weeks</Text>
				</View>
				<View style={styles.separator} />
			</ScrollView>
		);
	}
}

module.exports = DashboardPage;

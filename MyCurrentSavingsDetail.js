'use strict';

import React, { Component } from 'react'
import ToastModule from './src/nativ_modules/ToastModule'


import {
  View,
  TouchableHighlight,
  Text,
  TextInput,
  Keyboard,
  Alert,
  StyleSheet,
  TouchableOpacity
} from 'react-native';
import { StackNavigator } from 'react-navigation';

class MyCurrentSavingsDetail extends Component {
  static navigationOptions = {
    title: 'My current savings',
  };

  _testAlert = (arg1, arg2) => {
    Alert.alert("Today: " + Date());
  }

  _androidToast = () => {
    ToastModule.show("Hello Android World");
  }

  render() {
    
    const { navigate } = this.props.navigation;

    return (
      <View>
        <Text style={{ textAlign: 'center' }}>My current savings</Text>
        <Text style={{ textAlign: 'center' }} onPress={() => { this._testAlert(3, 4) }}>Alert</Text>

        <Text style={{ textAlign: 'center' }} onPress={() => { this._androidToast() }}>Android Nativ Toast</Text>

        <Text style={{ textAlign: 'center' }} onPress={() => navigate('IOSFileWriterTestPage') }>Go to IOSFileWriterTestPage</Text>

      </View>
    );
  }
}

module.exports = MyCurrentSavingsDetail;


var styles = StyleSheet.create({

  blockHyperLink: {
    flex: 1,
    fontSize: 12,
    paddingRight: 0,
    textAlign: 'right',
    textDecorationLine: 'underline',
    color: 'rgb(23, 125, 251)',
  },
  alignedFont: {
		fontFamily: 'System',
		textAlign: 'center',
		color: 'black'
	}
});

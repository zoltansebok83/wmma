import React from 'react';
import {
  AppRegistry,
  Text,
} from 'react-native';
import { StackNavigator } from 'react-navigation';

import DashboardPage from './DashboardPage';
import MyCurrentSavingsDetail from './MyCurrentSavingsDetail';
import IOSFileWriterTestPage from './IOSFileWriterTestPage';

const WMMA = StackNavigator({
  DashboardPage: { screen: DashboardPage },
  MyCurrentSavingsDetail: { screen: MyCurrentSavingsDetail},
  IOSFileWriterTestPage: { screen: IOSFileWriterTestPage}
});

AppRegistry.registerComponent('WMMA', () => WMMA);

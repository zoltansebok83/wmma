'use strict';

import { NativeModules } from 'react-native'

//call module method
export const showAndroidToast = (message) => {
    NativeModules.ToastModule.show(message)
  }

//available the module after import
  module.exports = NativeModules.ToastModule;

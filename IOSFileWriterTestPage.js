import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  TextInput,
  View,
  TouchableHighlight,
  Alert
} from 'react-native';
import { StackNavigator } from 'react-navigation';

var {
  // Demand in the auto-generated JavaScript class for our ObjC class
  MCFileWriterUtil
} = require('NativeModules');




class IOSFileWriterTestPage extends Component {

  static navigationOptions = {
    title: 'IOS File Writer Test Page',
  };

  constructor(props) {
    super(props);
    this.state = {
      textInputValue: '',
      resultsText: 'Nothing has happened yet :('
    };
  }

  render() {

    return (
      <View style={styles.container}>

        {/*  Container for isave and load buttons */}

        {/*  Container for input field */}
        <View style={styles.labelContainer}>
          <Text style={styles.labelText}>
            File Name :
                    </Text>
          <TextInput
            style={styles.textInput}
            ref="textInput"
            onChange={this._onTextInputChange}
          />
        </View>

        <View style={styles.buttonContainer}>
          <TouchableHighlight
            style={styles.touchableHighlight}
            underlayColor="#99AA99"
            onPress={this._onLoadPress}>
            <View style={[styles.buttonBox, styles.loadButtonBox]}>
              <Text style={styles.buttonText}>
                Load
                            </Text>
            </View>
          </TouchableHighlight>

          <TouchableHighlight
            underlayColor="#AA9999"
            onPress={this._onSavePress}>
            <View style={[styles.buttonBox, styles.saveButtonBox]}>
              <Text style={styles.buttonText}>
                Save
                                    </Text>
            </View>
          </TouchableHighlight>
        </View>

      </View>
    );
  }

  _onLoadPress = () => {

    var fileName = "test"


    MCFileWriterUtil.readFile(
      fileName,
      //errorCallback
      (results) => {
        alert('Error: ' + results.errMsg);
      },
      // successCallback
      (results) => {
        var resultsText = 'Contents of ' + fileName + ': ' + results.contents;

        alert('Contents of ' + fileName + ': ' + results.contents);

        // Update the state of the view
        this.setState({
          resultsText: resultsText
        });
      }
    );
  };


  _onSavePress = () => {

    var fileName = this.state.textInputValue,      // What file name to write?
      fileContents = Math.random().toString(); // This goes into the file

    MCFileWriterUtil.writeFile(
      fileName,
      fileContents,
      // errorCallback
      (results) => {
        alert('Error: ' + results.errMsg);
      },
      // successCallback
      (results) => {

        var resultsText = 'Saved to ' + fileName + '. Press load to see contents.';

        alert('Saved to ' + fileName + '. Press load to see contents.');

        // Update the state of the view
        this.setState({
          resultsText: resultsText
        });
      }
    );
  };


  // Handler for the TextInput onChange event
  _onTextInputChange = (event) => {
    this.setState({
      textInputValue: event.nativeEvent.text
    });
  };
}
module.exports = IOSFileWriterTestPage;



const styles = StyleSheet.create({
  
    container: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
      backgroundColor: '#F5FCFF',
    },
  
    buttonContainer: {
      flexDirection: 'row',
      justifyContent: 'center',
      alignItems: 'center',
      marginTop: 20
    },
  
    welcome: {
      fontSize: 20,
      textAlign: 'center',
      margin: 10,
    },
  
    instructions: {
      textAlign: 'center',
      color: '#333333',
      marginBottom: 5,
    },
  
    buttonBox: {
      flexDirection: 'row',
      justifyContent: 'center',
      alignItems: 'center',
      padding: 10,
      borderWidth: 2,
      borderRadius: 5
    },
  
    labelContainer: {
      flexDirection: 'row',
      width: 300
    },
  
    labelText: {
      paddingRight: 10,
      fontSize: 18
    },
  
    textInput: {
      height: 26,
      borderWidth: 0.5,
      borderColor: '#0f0f0f',
      padding: 4,
      flex: 1,
      fontSize: 13,
    },
  
    saveButtonBox: {
      borderColor: '#AA0000'
    },
  
    loadButtonBox: {
      borderColor: '#00AA00'
    },
    buttonText: {
      fontSize: 16,
    },
  });